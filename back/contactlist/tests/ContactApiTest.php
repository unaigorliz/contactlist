<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class CarApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  contact (
                id_contact	TEXT NOT NULL,
                name	TEXT,
                surname	TEXT,
                email	TEXT,
                mobile	TEXT,
            );
            INSERT INTO contact VALUES ('1','22', '3','red','KIA');
            INSERT INTO contact VALUES ('2','AAA002', '3','black','Dacia');
        ");
        // $this->withoutExceptionHandling();
    }

    public function testGetCars()
    {
        $this->json('GET', 'api/contact')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id_contact' => '1',
                    'name' => 'AAA001',
                    'surname' => '2019-01-01',
                    'email' => 'red',
                    'mobile' => 'KIA',
                ],
                [
                    'id_contact' => '2',
                    'name' => 'AAA002',
                    'surname' => '2019-01-02',
                    'email' => 'black',
                    'mobile' => 'Dacia',
                ],
            ]);
    }

    public function testGetCar()
    {
        $this->json('GET', 'api/contact/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'id_contact' => '1',
                    'name' => 'AAA001',
                    'surname' => '2019-01-01',
                    'email' => 'red',
                    'mobile' => 'KIA',
                ]
            );
    }

    public function testPostCar()
    {
        $this->json('POST', 'api/contact', [
            'id_contact' => '99',
            'name' => 'AAA002',
            'surname' => '2019-01-02',
            'email' => 'black',
            'make' => 'KIA',
            'mobile' => 'Carens',
        ])->assertStatus(200);

        $this->json('GET', 'api/contact/99')
            ->assertStatus(200)
            ->assertJson([
                'id' => '99',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);
    }

    public function testPut()
    {
        $data = [
            'id' => '1',
            'registration' => 'AAA002',
            'dateRegistered' => '2019-01-02',
            'color' => 'black',
            'make' => 'KIA',
            'model' => 'Carens',
        ];

        $expected = [
            'id' => '1',
            'registration' => 'AAA002',
            'dateRegistered' => '2019-01-02',
            'color' => 'black',
            'make' => 'KIA',
            'model' => 'Carens',
        ];

        $this->json('PUT', 'api/cars/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/cars/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

    public function testPatch()
    {
        $this->json('PATCH', 'api/cars/1', [
            'registration' => 'AAA002',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-01',
                'color' => 'red',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);

        $this->json('GET', 'api/cars/1')
            ->assertStatus(200)
            ->assertJson([
                'registration' => 'AAA002',
            ]);

        $this->json('PATCH', 'api/cars/1', [
            'dateRegistered' => '2019-01-02',
            'color' => 'black',
            'make' => 'KIA',
            'model' => 'Carens',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);

        $this->json('GET', 'api/cars/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);
    }

    public function testDeleteCar()
    {
        $this->json('GET', 'api/cars/1')->assertStatus(200);

        $this->json('DELETE', 'api/cars/1')->assertStatus(200);

        $this->json('GET', 'api/cars/1')->assertStatus(404);
    }

    public function testGetCarNotExist()
    {
        // $this->json('GET', 'api/cars/22')->assertStatus(404);

        $this->json('DELETE', 'api/cars/22')->assertStatus(404);

        $this->json('PUT', 'api/cars/22', 
            [
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/cars/22', [
            'registration' => 'AAA002',
        ])
            ->assertStatus(404);
    }

}