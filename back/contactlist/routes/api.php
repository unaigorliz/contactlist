<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/contacts', function (Request $request) {
    $results = DB::select('select * from contact    ');
    return response()->json($results, 200);
});
/**************************************************CREATE***********************************************************/
Route::post('/contacts/', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();
    /*echo($data);*/
    DB::insert(
        "
          insert into contact (id_contact, name, surname, email, mobile)
          values ('" . $data['id_contact'] . "', '" . $data['name'] . "', '" . $data['surname'] . "', '" . $data['email'] . "', '" . $data['mobile'] . "')
        "
    );
    $results = DB::select('select * from contact where id_contact = :id_contact', [
        'id_contact' => $data['id_contact'],
    ]);
    return response()->json($results[0], 200);
});
/***************************************************************DELETE*****************************************************/
Route::delete('/contacts/{id}', function ($id_contact) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    DB::delete('delete from contact where id_contact = :id_contact', ['id_contact' => $id_contact]);
    return response()->json('', 200);
   // $this->withoutExceptionHandling();
});
/***************************************************************UPDATE*****************************************************/
Route::put('/contacts/{id_contact}', function ($id_contact) {

    $data = request()->all();

    DB::delete(
        "
        delete from contact where id_contact = :id_contact " , ['id_contact' => $id_contact]
    );
    DB::insert(
        "
        insert into contact (id_contact, name, surname, email, mobile)
        values (:id_contact, :name, :surname, :email, :mobile)
    ",
        $data
    );

    $results = DB::select('select * from contact where id_contact = :id_contact', ['id_contact' => $id_contact]);
    return response()->json($results[0], 200);
});