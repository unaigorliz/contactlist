import { shallowMount } from '@vue/test-utils'
import ContactItem from '@/components/ContactItem.vue'

test('Comprobamos que pinta con una clase', () => {
    //crear componente con las propiedades
    //comprobar que el nombre y el apellido estan en su sitio
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { name: 'Sergio', surname: 'Bombien' },
        },
    })
    //buscamos la clase de la plantilla html (find)
    const fullNameTemplate = wrapper.find('.full-name')

    expect(fullNameTemplate.text()).toContain('Sergio Bombien')
})

test('Comprobamos que la información del padre', () => {
    //crear componente con las propiedades
    //comprobar que el nombre y el apellido estan en su sitio
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { name: 'Unai', surname: 'Madariaga' },
        },
    })
    //buscamos la clase de la plantilla html (find)
    const fullnameTemplate = wrapper.find('.full-name')

    expect(fullnameTemplate.text()).toContain('Unai Madariaga')
})

//test de eventos
//-------------------show-------------------//
test('Comprobar evento buttonShow', async () => {
    //crear componente con las propiedades
    //comprobar eventos
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { id: '1', name: 'Sergio', surname: 'Bombien' },
        },
    })
    //comprobar el tamaño del array de eventos emitidos
    expect(wrapper.emittedByOrder()).toEqual([])

    const buttonShow = wrapper.find('.show-button')
    buttonShow.trigger('click') //simula evento click
    await wrapper.vm.$nextTick() //magia vue
    //compara dos arrays

    expect(wrapper.emittedByOrder()).toEqual([
        {
            name: 'show',
            args: [{ id: '1', name: 'Sergio', surname: 'Bombien' }],
        },
    ])
})

//-------------------update-------------------//
test('Comprobar evento buttonUpdate', async () => {
    //crear componente con las propiedades
    //comprobar eventos
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { id: '1', name: 'Sergio', surname: 'Bombien' },
        },
    })
    //comprobar el tamaño del array de eventos emitidos
    expect(wrapper.emittedByOrder()).toEqual([])

    const buttonUpdate = wrapper.find('.update-button')

    buttonUpdate.trigger('click')
    //simula evento click
    await wrapper.vm.$nextTick() //magia vue

    //compara dos arrays
    expect(wrapper.emittedByOrder()).toEqual([
        {
            name: 'update',
            args: [{ id: '1', name: 'Sergio', surname: 'Bombien' }],
        },
    ])
})
//-------------------erase-------------------//

test('Comprobar evento buttonErase', async () => {
    //crear componente con las propiedades
    //comprobar eventos
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { id: '1', name: 'Sergio', surname: 'Bombien' },
        },
    })
    //comprobar el tamaño del array de eventos emitidos
    expect(wrapper.emittedByOrder()).toEqual([])

    const buttonErase = wrapper.find('.erase-button')

    buttonErase.trigger('click')

    await wrapper.vm.$nextTick() //magia vue
    //compara dos arrays

    expect(wrapper.emittedByOrder()).toEqual([
        {
            name: 'erase',
            args: [{ id: '1', name: 'Sergio', surname: 'Bombien' }],
        },
    ])
})

//-------------------computed-------------------//

test('compputed fullname', async () => {
    //crear componente con las propiedades
    //comprobar eventos
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: { id: '1', name: 'Sergio', surname: 'Bombien' },
        },
    })
    const expected = wrapper.vm.fullName
    expect(expected).toBe('Sergio Bombien')

    //    wrapper.setProps({
    //      contact: { id: '1', name: 'Unai', surname: 'Madariaga' },
    //  })
    //expect(wrapper.vm.fullName).toBe('Unai Madariaga')
})
