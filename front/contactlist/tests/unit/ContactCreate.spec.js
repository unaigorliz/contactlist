import { shallowMount } from '@vue/test-utils'
import ContactCreate from '@/components/ContactCreate.vue'

test('Comprobamos que pinta los inputs', () => {
    //crear componente con las propiedades
    //comprobar que el nombre y el apellido estan en su sitio
    const wrapper = shallowMount(ContactCreate, {
        propsData: { selectedContact: { type: Object, required: true } },
    })
    //buscamos la clase de la plantilla html (find)
    const inputContactCreate = wrapper.findAll('.inputContactCreate').wrappers
    expect(inputContactCreate.length).toEqual(4)
})

//test de eventos
//-------------------save-------------------//
test('Comprobar evento crear', async () => {
    //crear componente con las propiedades
    //comprobar eventos
    const wrapper = shallowMount(ContactCreate, {
        propsData: { selectedContact: { name: null, surname: null, mail: null, mobile: null } },
    })
    //comprobar el tamaño del array de eventos emitidos
    expect(wrapper.emittedByOrder()).toEqual([])

    const saveButton = wrapper.find('.SaveButton')
    saveButton.trigger('click') //simula evento click
    await wrapper.vm.$nextTick() //magia vue

    // //compara dos arrays

    expect(wrapper.emittedByOrder()).toEqual([
        { name: 'clickSaveButton', args: [wrapper.vm.localContact, 'new'] },
    ])
})
