import { shallowMount } from "@vue/test-utils";
import ContactList from "@/components/ContactList.vue";

test("Comprobar el componente ContactList", () => {
  const wrapper = shallowMount(ContactList, {
    propsData: {
      contactList: [
        {
          id: "1",
          name: "pakito",
          surname: "pil",
          mobile: "666999666",
          mail: "racapower@eu.com"
        },
        {
          id: "2",
          name: "ermenegildo",
          surname: "ruperez",
          mobile: "545654897",
          mail: "azukiki@eu.com"
        }
      ]
    }
  });
  //buscamos la clase de la plantilla html o el componente  con (findAll)

  const contactItems = wrapper.findAll(".contactItem").wrappers;
  //para acceder dentro del wrapper usamos .vm
  expect(contactItems.length).toBe(2);
  expect(contactItems[0].vm.contact.id).toBe("1");
  expect(contactItems[1].vm.contact.id).toBe("2");
});

test("emite evento click createcontact", async () => {
  const wrapper = shallowMount(ContactList, {
    
    propsData: {
      contactList: [
        {
          id: "1",
          name: "pakito",
          surname: "pil",
          mobile: "666999666",
          mail: "racapower@eu.com"
        },
        {
          id: "2",
          name: "ermenegildo",
          surname: "ruperez",
          mobile: "545654897",
          mail: "azukiki@eu.com"
        }
      ]
    }
  });

  expect(wrapper.emittedByOrder()).toEqual([]);
  const classCreateButton = wrapper.find(".create-button");

  classCreateButton.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emittedByOrder()).toEqual([{ name: 'newContact', args: [] },])
});
