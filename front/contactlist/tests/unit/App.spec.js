import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'

test('si funciona la function getContactList ', async () => {
    const MockApiCall = jest.fn()
    const mockContacts = [
        {
            id: '01',
            name: 'Diegos',
            surname: 'ajami',
            email: 'Diegos.my@Diegos.com',
            mobile: '631125578',
        },
        {
            id: '02',
            name: 'diego',
            surname: 'espinosa',
            email: 'diego.esp@Diegos.com',
            mobile: '63254789',
        },
    ]
    MockApiCall.mockReturnValue(mockContacts)
    const wrapper = shallowMount(App, {
        data() {
            return {
                selectedContact: null,
            }
        },
        methods: {
            loadContactList: MockApiCall,
        },
    })
    await wrapper.vm.$nextTick()
    expect(MockApiCall).toHaveBeenCalled()
    expect(wrapper.vm.contactos).toEqual(mockContacts)
})
