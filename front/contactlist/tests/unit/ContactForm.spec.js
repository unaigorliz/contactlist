import { shallowMount } from '@vue/test-utils'
import ContactForm from '@/components/ContactForm.vue'

test('Comprobamos que pinta por clase', () => {
    //crear componente con las propiedades
    //comprobar que el nombre y el apellido estan en su sitio
    const wrapper = shallowMount(ContactForm, {
        propsData: {
            contact: {
                id: '1',
                name: 'Sergio',
                surname: 'Bombien',
                mobile: '641123123',
                mail: 'sergioqueteden@money.com',
            },
        },
    })
    //buscamos la clase de la plantilla html (find)
    const fullNameTemplate = wrapper.find('.form')
    console.log('fullNameTemplate', fullNameTemplate)

    expect(fullNameTemplate.text()).toContain('Sergio')
})

test('Comprobamos que pintan las propiedades', () => {
    //crear componente con las propiedades
    //comprobar que el nombre y el apellido estan en su sitio
    const wrapper = shallowMount(ContactForm, {
        propsData: {
            contact: {
                id: '1',
                name: 'Sergio',
                surname: 'Bombien',
                mobile: '641123123',
                mail: 'sergioqueteden@money.com',
            },
        },
    })
    //buscamos la clase de la plantilla html (find)
    const nameTemplate = wrapper.find('.name')
    expect(nameTemplate.text()).toBe('Sergio')

    const surNameTemplate = wrapper.find('.surname')
    expect(surNameTemplate.text()).toBe('Bombien')

    const mobileTemplate = wrapper.find('.mobile')
    expect(mobileTemplate.text()).toBe('641123123')

    const mailTemplate = wrapper.find('.mail')
    expect(mailTemplate.text()).toBe('sergioqueteden@money.com')
})
